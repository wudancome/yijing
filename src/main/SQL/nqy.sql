/*
 Navicat Premium Data Transfer

 Source Server         : 本地数据库
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : tang

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 23/11/2021 16:08:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for nqy
-- ----------------------------
DROP TABLE IF EXISTS `nqy`;
CREATE TABLE `nqy`  (
  `jiaji` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '甲己之年丙做首',
  `yigeng` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '乙庚之岁戊为头',
  `bingxin` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '丙辛岁首寻庚起',
  `dingren` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '丁壬壬位顺行流',
  `wugui` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '若言戊癸何方发，甲寅之上好追求',
  `yuefen` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '月份',
  `id` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nqy
-- ----------------------------
INSERT INTO `nqy` VALUES ('丙寅', '戊寅', '庚寅', '壬寅', '甲寅', '正月', 0);
INSERT INTO `nqy` VALUES ('丁卯', '己卯', '辛卯', '癸卯', '乙卯', '2月', 1);
INSERT INTO `nqy` VALUES ('戊辰', '庚辰', '壬辰', '甲辰', '丙辰', '3月', 2);
INSERT INTO `nqy` VALUES ('己巳', '辛巳', '癸巳', '乙巳', '丁巳', '4月', 3);
INSERT INTO `nqy` VALUES ('庚午', '壬午', '甲午', '丙午', '戊午', '5月', 4);
INSERT INTO `nqy` VALUES ('辛未', '癸未', '乙未', '丁未', '己未', '6月', 5);
INSERT INTO `nqy` VALUES ('壬申', '甲申', '丙申', '戊申', '庚申', '7月', 6);
INSERT INTO `nqy` VALUES ('癸酉', '乙酉', '丁酉', '己酉', '辛酉', '8月', 7);
INSERT INTO `nqy` VALUES ('甲戌', '丙戌', '戊戌', '庚戌', '壬戌', '9月', 8);
INSERT INTO `nqy` VALUES ('乙亥', '丁亥', '己亥', '辛亥', '癸亥', '10月', 9);
INSERT INTO `nqy` VALUES ('丙子', '戊子', '庚子', '壬子', '甲子', '11月', 10);
INSERT INTO `nqy` VALUES ('丁丑', '己丑', '辛丑', '癸丑', '乙丑', '12月', 11);

SET FOREIGN_KEY_CHECKS = 1;
