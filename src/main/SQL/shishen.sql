/*
 Navicat Premium Data Transfer

 Source Server         : 本地数据库
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : tang

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 23/11/2021 16:08:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for shishen
-- ----------------------------
DROP TABLE IF EXISTS `shishen`;
CREATE TABLE `shishen`  (
  `id` int(10) NOT NULL,
  `jia` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日主天干',
  `yi` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '八字天干',
  `bing` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '神煞',
  `ding` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `wu` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ji` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `geng` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `xin` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ren` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gui` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shishen
-- ----------------------------
INSERT INTO `shishen` VALUES (0, '比肩', '劫财', '食神', '伤官', '偏财', '正财', '偏官', '正官', '偏印', '正印');
INSERT INTO `shishen` VALUES (1, '劫财', '比肩', '伤官', '食神', '正财', '偏财', '正官', '偏官', '正印', '偏印');
INSERT INTO `shishen` VALUES (2, '偏印', '正印', '比肩', '劫财', '食神', '伤官', '偏官', '正官', '偏印', '正印');
INSERT INTO `shishen` VALUES (3, '正印', '偏印', '劫财', '比肩', '伤官', '食神', '正财', '偏财', '正官', '偏官');
INSERT INTO `shishen` VALUES (4, '偏官', '正官', '偏印', '正印', '比肩', '劫财', '食神', '伤官', '偏财', '正财');
INSERT INTO `shishen` VALUES (5, '正官', '偏官', '正印', '偏印', '劫财', '比肩', '伤官', '食神', '正财', '偏财');
INSERT INTO `shishen` VALUES (6, '偏财', '正财', '偏官', '正官', '偏印', '正印', '比肩', '劫财', '食神', '伤官');
INSERT INTO `shishen` VALUES (7, '正财', '偏财', '正官', '偏官', '正印', '偏印', '劫财', '比肩', '伤官', '食神');
INSERT INTO `shishen` VALUES (8, '食神', '伤官', '偏财', '正财', '偏官', '正官', '偏印', '正印', '比肩', '劫财');
INSERT INTO `shishen` VALUES (9, '伤官', '食神', '正财', '偏财', '正官', '偏官', '正印', '偏印', '劫财', '比肩');

SET FOREIGN_KEY_CHECKS = 1;
