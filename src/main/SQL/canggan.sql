/*
 Navicat Premium Data Transfer

 Source Server         : 本地数据库
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : tang

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 23/11/2021 16:07:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for canggan
-- ----------------------------
DROP TABLE IF EXISTS `canggan`;
CREATE TABLE `canggan`  (
  `id` int(10) NOT NULL,
  `cang` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '藏干',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of canggan
-- ----------------------------
INSERT INTO `canggan` VALUES (0, '癸');
INSERT INTO `canggan` VALUES (1, '己癸辛');
INSERT INTO `canggan` VALUES (2, '甲丙戊');
INSERT INTO `canggan` VALUES (3, '乙');
INSERT INTO `canggan` VALUES (4, '戊乙癸');
INSERT INTO `canggan` VALUES (5, '丙戊庚');
INSERT INTO `canggan` VALUES (6, '丁己');
INSERT INTO `canggan` VALUES (7, '己丁乙');
INSERT INTO `canggan` VALUES (8, '庚壬戊');
INSERT INTO `canggan` VALUES (9, '辛');
INSERT INTO `canggan` VALUES (10, '戊辛丁');
INSERT INTO `canggan` VALUES (11, '壬甲');

SET FOREIGN_KEY_CHECKS = 1;
