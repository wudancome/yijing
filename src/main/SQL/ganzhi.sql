/*
 Navicat Premium Data Transfer

 Source Server         : 本地数据库
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : tang

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 23/11/2021 16:08:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ganzhi
-- ----------------------------
DROP TABLE IF EXISTS `ganzhi`;
CREATE TABLE `ganzhi`  (
  `tiangan` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '天干',
  `dizhi` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地支',
  `id` int(10) NOT NULL COMMENT '序号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ganzhi
-- ----------------------------
INSERT INTO `ganzhi` VALUES ('甲', '子', 0);
INSERT INTO `ganzhi` VALUES ('乙', '丑', 1);
INSERT INTO `ganzhi` VALUES ('丙', '寅', 2);
INSERT INTO `ganzhi` VALUES ('丁', '卯', 3);
INSERT INTO `ganzhi` VALUES ('戊', '辰', 4);
INSERT INTO `ganzhi` VALUES ('己', '巳', 5);
INSERT INTO `ganzhi` VALUES ('庚', '午', 6);
INSERT INTO `ganzhi` VALUES ('辛', '未', 7);
INSERT INTO `ganzhi` VALUES ('壬', '申', 8);
INSERT INTO `ganzhi` VALUES ('癸', '酉', 9);
INSERT INTO `ganzhi` VALUES ('甲', '戌', 10);
INSERT INTO `ganzhi` VALUES ('乙', '亥', 11);

SET FOREIGN_KEY_CHECKS = 1;
