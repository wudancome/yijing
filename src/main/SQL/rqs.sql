/*
 Navicat Premium Data Transfer

 Source Server         : 本地数据库
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : tang

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 23/11/2021 16:08:27
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for rqs
-- ----------------------------
DROP TABLE IF EXISTS `rqs`;
CREATE TABLE `rqs`  (
  `jiaji` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '甲己还加甲',
  `yigeng` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '乙庚是丙初',
  `bingxin` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '丙辛从戊起',
  `dingren` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '丁壬居庚子',
  `wugui` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '戊癸在何方，壬子是真途',
  `shichen` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '时辰',
  `id` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rqs
-- ----------------------------
INSERT INTO `rqs` VALUES ('甲子', '丙子', '戊子', '庚子', '壬子', '子时', 0);
INSERT INTO `rqs` VALUES ('乙丑', '丁丑', '己丑', '辛丑', '癸丑', '丑时', 1);
INSERT INTO `rqs` VALUES ('丙寅', '戊寅', '庚寅', '壬寅', '甲寅', '寅时', 2);
INSERT INTO `rqs` VALUES ('丁卯', '己卯', '辛卯', '癸卯', '乙卯', '卯时', 3);
INSERT INTO `rqs` VALUES ('戊辰', '庚辰', '壬辰', '甲辰', '丙辰', '辰时', 4);
INSERT INTO `rqs` VALUES ('己巳', '辛巳', '癸巳', '乙巳', '丁巳', '巳时', 5);
INSERT INTO `rqs` VALUES ('庚午', '壬午', '甲午', '丙午', '戊午', '午时', 6);
INSERT INTO `rqs` VALUES ('辛未', '癸未', '乙未', '丁未', '己未', '未时', 7);
INSERT INTO `rqs` VALUES ('壬申', '甲申', '丙申', '戊申', '庚申', '申时', 8);
INSERT INTO `rqs` VALUES ('癸酉', '乙酉', '丁酉', '己酉', '辛酉', '酉时', 9);
INSERT INTO `rqs` VALUES ('甲戌', '丙戌', '戊戌', '庚戌', '壬戌', '戌时', 10);
INSERT INTO `rqs` VALUES ('乙亥', '丁亥', '己亥', '辛亥', '癸亥', '亥时', 11);

SET FOREIGN_KEY_CHECKS = 1;
