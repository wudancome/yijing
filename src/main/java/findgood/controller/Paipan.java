package findgood.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import findgood.entity.nqy;
import findgood.entity.rqs;
import findgood.service.Bazi;
import findgood.tools.GanZhi;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@RestController
@RequestMapping("/bazi")
public class Paipan{
	@Autowired
	private Bazi bazi;
	@Autowired
	private nqy nqy;//年上起月
	@Autowired
	private rqs rqs;//日上起时
	@PostMapping("/paipan")//排盘,农历闰月，对应的时间计算方法
	public String shengchen(int Nyue,int Ynian,int Yyue,int Yri,int Yshi){
		log.info("传入生辰八字！农历："+Ynian+"年"+Nyue+"月");
		int cha = Ynian-1924;
		int tiangan = cha % 10;
		int dizhi = cha % 12;
		String nianZhu=getTianGan(tiangan)+getDiZhi(dizhi);
		String yueZhu=bazi.selectYuezhu(Nyue-1, tiangan);
		
		//废弃的方法
		int rizhuNumber= GanZhi.YLgetRZ(Ynian, Yyue, Yri);//传入阳历的年月日，得到日柱的天干序号
		String riZhu=bazi.selectRizhu(rizhuNumber);
		log.info("rizhuNumber:"+rizhuNumber+"\t日柱干支："+riZhu);
		String shiZhu=bazi.selectShizhu(riZhu, Yshi);
		log.info("八字: ："+nianZhu	+"-"+yueZhu+"-"+riZhu+"-"+shiZhu);
		//根据公历推算当日干支 ；	乘五除四九加日，双月间隔三十天。 一二自加整少一，三五七八十尾前。
		return "八字: ："+nianZhu	+"-"+yueZhu+"-"+riZhu+"-"+shiZhu;
	}
	@PostMapping("/createnqy")
	public void createnqy(){
		log.info("开始保存年上起月表！");
		for (int i = 0; i <12; i++) {
			nqy.setId(i);
			String yue="";
			if (i==0) {
				yue="正月";
				nqy.setYuefen("正月");
			}else {
				yue=(i+1)+"月";
				nqy.setYuefen((i+1)+"月");
			}
			int dizhiXuhao=(i+2)%12;//寅做首。固定
			log.trace("地支序号"+dizhiXuhao);
			String diZi=getDiZhi(dizhiXuhao);
			
			int  jiajiXuhao=(i+2)%10;//甲己丙做首
			nqy.setJiaji(getTianGan(jiajiXuhao)+diZi);
			int  yigengXuhao=(i+4)%10;//乙庚戊为头
			nqy.setYigeng(getTianGan(yigengXuhao)+diZi);
			int  BingxinXuhao=(i+6)%10;//丙辛寻庚起
			nqy.setBingxin(getTianGan(BingxinXuhao)+diZi);
			int  DingrenXuhao=(i+8)%10;//丁壬壬顺流
			nqy.setDingren(getTianGan(DingrenXuhao)+diZi);
			int  WuguiXuhao=(i+10)%10;//戊癸甲上求
			nqy.setWugui(getTianGan(WuguiXuhao)+diZi);
			log.info(i+" "+yue+"\t"+
			getTianGan(jiajiXuhao)+diZi+" "+
			getTianGan(yigengXuhao)+diZi+" "+
			getTianGan(BingxinXuhao)+diZi+" "+
			getTianGan(DingrenXuhao)+diZi+" "+
			getTianGan(WuguiXuhao)+diZi);
//			bazi.savenqy(nqy);
		}
	}
	@PostMapping("/createrqs")
	public void createrqs(){
		log.info("开始保存日上起时表！");
		for (int i = 0; i <12; i++) {
			rqs.setId(i);
			String diZi=getDiZhi(i);
			rqs.setShichen(diZi+"时");
			int  jiajiXuhao=i%10;//甲己还加甲
			rqs.setJiaji(getTianGan(jiajiXuhao)+diZi);
			int  yigengXuhao=(i+2)%10;//乙庚是丙初
			rqs.setYigeng(getTianGan(yigengXuhao)+diZi);
			int  BingxinXuhao=(i+4)%10;//丙辛从戊起
			rqs.setBingxin(getTianGan(BingxinXuhao)+diZi);
			int  DingrenXuhao=(i+6)%10;//丁壬居庚子
			rqs.setDingren(getTianGan(DingrenXuhao)+diZi);
			int  WuguiXuhao=(i+8)%10;//戊癸在何方, 壬子是真途
			rqs.setWugui(getTianGan(WuguiXuhao)+diZi);
			log.info(i+" "+diZi+"时"+"\t"+
			getTianGan(jiajiXuhao)+diZi+" "+
			getTianGan(yigengXuhao)+diZi+" "+
			getTianGan(BingxinXuhao)+diZi+" "+
			getTianGan(DingrenXuhao)+diZi+" "+
			getTianGan(WuguiXuhao)+diZi);
//			bazi.saverqs(rqs);
		}
	}
	public  String getTianGan(int number){
		String tiangan= bazi.selectTiangan(number);
		return tiangan;
	}
	public String getDiZhi(int number){
		String diZi= bazi.selectDizhi(number);
		return diZi;
	}
}
