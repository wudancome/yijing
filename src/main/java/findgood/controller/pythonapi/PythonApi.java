package findgood.controller.pythonapi;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.heqiao2010.lunar.LunarCalendar;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@RequestMapping("/huoqu")
public class PythonApi {

    @PostMapping("/nongli")
    public Object getNongli(String nyri) {

        System.out.print("请输入阳历日期 (yyyy-MM-dd): ");
//        String dateInput = "2024-11-13";

        // 将输入的字符串解析为阳历日期
        java.time.LocalDate solarDate = java.time.LocalDate.parse(nyri);

        // 使用 LunarCalendar 获取农历日期
        LunarCalendar lunarCalendar = new LunarCalendar(solarDate.getYear(), solarDate.getMonthValue(), solarDate.getDayOfMonth());

        // 输出农历日期
        System.out.println("农历日期: " + lunarCalendar.getLunarYear() + "年 " +
                (lunarCalendar.getLunarMonth()-1) + "月 " +
                lunarCalendar.getDayOfLunarMonth() + "日 " +
                (lunarCalendar.isLeapMonth() ? "(闰月)" : "不闰月"));
        String result = lunarCalendar.getLunarYear() + "年" +
                (lunarCalendar.getLunarMonth()-1) + "月" +
                lunarCalendar.getDayOfLunarMonth() + "日" ;
        return result;
    }

    public static void main(String[] args) {
        PythonApi pythonApi = new PythonApi();
        pythonApi.getNongli("1996-10-28");
        pythonApi.getNongli("2022-12-09");
    }
    public  String xiangxi(String nyri) {

        return "result";
    }
}
