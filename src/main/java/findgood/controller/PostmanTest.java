package findgood.controller;

import java.util.Map;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import findgood.tools.ObjectFactory;
import findgood.tools.photoToExcel;
import lombok.extern.slf4j.Slf4j;
/**
 * @author 唐春
 * @version 1.0 2021年6月30日 下午4:39:44 
 * @since JDK 1.8
 * 	使用postman测试；为接口测试写的，不参与身份证替换系统的功能，只是借这个地址，在postman上面进行调试
 */
@Slf4j
@RestController
@RequestMapping("/a")
public class PostmanTest {
	/**
	 * @author 唐春  2021年6月30日 下午4:36:43
	 * @param name 姓名
	 * @return Code 返回值
	 */
	@PostMapping("/b")
	public void getExcel() {
		try {
			photoToExcel.suan();
		} catch (Exception e) {
			System.out.println("HTTP状态码开头为：1、2、3、4、5的时候");
			System.out.println("分别代表什么意思?");
		}	
	}
	/**
	 * 	用来测试日志生成的文件格式以及大小
	 * @author 唐春  2021年6月30日 下午4:43:42
	 */
	@PostMapping("/d")
	public void show() {
		Map<String, String> data =ObjectFactory.getHashMap();
		data.put("zha", "这是空数据测试");
		log.trace("trace描述日志");
		log.info("info通知日志");
		log.debug("debug调试日志");
		log.warn("warn警告日志");
		for (int i = 0; i <10000000; i++) {
			log.info("这是info级别的日志");
			log.trace("这是trace级别的日志");
			log.debug("这是debug级别的日志");
			log.warn("这是warn级别的日志");
		}
	}
	/**
	 * 
	 * @author 唐春  2021年6月30日 下午4:44:09
	 * @param id	用户id
	 * @param pwd	密码
	 */
	@GetMapping("/c")
	public void Download(String id,String pwd){
		Map<String, String> map= ObjectFactory.getHashMap();
		map.put("a", id);
		map.put("b", pwd);
      }
}
