package findgood.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import findgood.service.Yongshen;
import findgood.tools.GanZhi;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@RestController
@RequestMapping("/yongshen")
public class Quyongshen{
	
	@Autowired
	private Yongshen yon;
	@PostMapping("/qu")
	public Map<String, Object> qu(String a,String b,String c,String d){
		log.info("传进来的八字："+a+b+c+d);
		String riYuan=c.substring(0,1);int riyuan=0;
		riyuan=GanZhi.getGan(riYuan, riyuan);
		Map<String,Object> resultMap=new HashMap<String, Object>();
		List<String> first=new ArrayList<String>();
		List<String> second=new ArrayList<String>();
		List<String> third=new ArrayList<String>();
		List<String> four=new ArrayList<String>();
		List<String> five=new ArrayList<String>();
			first.add(yon.selectshen(riyuan, a.substring(0,1)));			first.add(yon.selectshen(riyuan, b.substring(0,1)));
			first.add("日元");			first.add(yon.selectshen(riyuan, d.substring(0,1)));
		second.add(a.substring(0,1));second.add(b.substring(0,1));second.add(c.substring(0,1));second.add(d.substring(0,1));
		third.add(a.substring(1));third.add(b.substring(1));third.add(c.substring(1));third.add(d.substring(1));
		List<String> fourDizhi=new ArrayList<String>();
		fourDizhi.add(a.substring(1));fourDizhi.add(b.substring(1));fourDizhi.add(c.substring(1));fourDizhi.add(d.substring(1));
		List<List<String>> cangGan=new ArrayList<List<String>>();
		List<List<String>> diZhiShen=new ArrayList<List<String>>();
		for (int i = 0; i <4; i++) {
			int di=GanZhi.getZhi(fourDizhi.get(i));
			List<String> cangGanx=new ArrayList<String>();
			List<String> diZhiShenx=new ArrayList<String>();
			String cang= yon.seleCanggan(di);
			for (int j = 0; j < cang.length(); j++) {
				String dizh= cang.substring(j,j+1);
				String cangShen= yon.selectshen(riyuan,dizh);
				cangGanx.add(dizh);
				diZhiShenx.add(cangShen);
			}
			cangGan.add(cangGanx);
			diZhiShen.add(diZhiShenx);
		}
		for (int i = 0; i <cangGan.size(); i++) {
			String canggan="";
			for (int j = 0; j <cangGan.get(i).size(); j++) {
				canggan+=cangGan.get(i).get(j);
			}
			four.add(canggan);
		}
		for (int i = 0; i <diZhiShen.size(); i++) {
			String cangShen="";
			for (int j = 0; j <diZhiShen.get(i).size(); j++) {
				cangShen+=diZhiShen.get(i).get(j);
			}
			five.add(cangShen);
		}
		//输出日柱藏干
		resultMap.put("first", first);
		resultMap.put("second", second);
		resultMap.put("third", third);
		resultMap.put("four", four);
		resultMap.put("five", five);
		Integer monthY=GanZhi.getYueFen(b.substring(1));
		String tizhuTianGan=c.substring(0,1);
		String th=yon.selethshen(monthY,tizhuTianGan);
		resultMap.put("th", "调候用神："+th);
		String thshen="";
		for (int i = 0; i <th.length(); i++) {
			String dizh= th.substring(i,i+1);
			String cangShen= yon.selectshen(riyuan,dizh);
			thshen+=cangShen+"、";
		}
		resultMap.put("thshen",thshen);
		return resultMap;
	}
}