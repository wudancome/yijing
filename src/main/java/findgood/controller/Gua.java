package findgood.controller;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import findgood.service.impl.GuaImpl;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@RestController
@RequestMapping("/gua")
public class Gua {
	@Autowired
	private GuaImpl Guaimpl;
	@PostMapping("/getone")
	public Map<String, Object> OneGua(String a){
		return Guaimpl.selceGuaci(a);
	}
	@PostMapping("/order")
	public Map<String, Object> chooseGua(int order) {
		return Guaimpl.chooseGuaorder(order);
	}
	@PostMapping("/name")
	public Map<String, Object> chooseGuaname(String name) {
		return Guaimpl.chooseGuaname(name);
	}
	@PostMapping("/savegua")
	public int saveGua(MultipartFile file){
		int a=Guaimpl.fileSaveGua(file);
		log.info("保存卦辞成功！");
		return a;
	}
	@PostMapping("/saveyao")
	public int saveYao(MultipartFile file) {
		int a=Guaimpl.fileSaveYao(file);
		log.info("保存爻辞成功！");
		return a;
	}
	@PostMapping("/savenengliang")
	public int saveNengLiang(MultipartFile file) {
		int a=Guaimpl.fileSaveNengLiang(file);
		log.info("保存能量表成功！");
		return a;
	}
}
