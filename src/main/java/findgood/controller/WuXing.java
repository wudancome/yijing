package findgood.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import findgood.service.Nengliang;
import lombok.extern.slf4j.Slf4j;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/neng")
public class WuXing {
	@Autowired
	private Nengliang nengliang;
	@PostMapping("/bazi")
	public Map<String, Object> bazi(String dayun, String liunian, String a, String b, String c, String d){
		Map<String, Object> resultMap=new HashMap<String, Object>();
		try {
			List<String> res=nengliang.Ming(a, b, c, d);
			resultMap = getData("八字本身", res);
		} catch (Exception e) {
			resultMap =wrongParameter(liunian);
			e.printStackTrace();
		}
		return resultMap;
	}
	@PostMapping("/dayun")
	public Map<String, Object> dayun(String dayun, String liunian, String a, String b, String c, String d) {
		List<String> res=nengliang.Yun(dayun, a, b, c, d);
		Map<String,Object> resultMap=getData("只包含["+dayun+"]大运", res);
		return resultMap;
	}
	@PostMapping("/liunian")
	public Map<String, Object> liunian(String dayun, String liunian, String a, String b, String c, String d) {
		List<String> res=nengliang.Nian(dayun, liunian, a, b, c, d);
		Map<String,Object> resultMap=getData(liunian+"流年", res);
		return resultMap;
	}
	@PostMapping("/yunsuan")
	public Map<String, Object> yunsuan(Integer leixing, String dayun, String liunian, String a, String b, String c, String d) {
		Map<String, Object> resultMap=new HashMap<String, Object>();
		try {
			resultMap = nengliang.first(leixing, dayun, liunian, a, b, c, d);
			nengliang.second(resultMap);
			nengliang.third(resultMap);
			nengliang.four(resultMap);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return resultMap;
	}
	
	public static Map<String, Object> getData(String liunian,List<String> res) {
		Map<String,Object> resultMap=new HashMap<String, Object>();
		resultMap.put("liunian",liunian);
		resultMap.put("jin",  res.get(0));
		resultMap.put("shui",  res.get(1));
		resultMap.put("mu",  res.get(2));
		resultMap.put("huo", res.get(3));
		resultMap.put("tu",  res.get(4));
		resultMap.put("total", res.get(5));
	return resultMap;
}
	public static Map<String, Object> wrongParameter(String liunian) {
		Map<String,Object> resultMap=new HashMap<String, Object>();
		resultMap.put("liunian",  "检查");
		resultMap.put("jin",  "八字");
		resultMap.put("shui",  "参数");
		resultMap.put("mu",   "是否");
		resultMap.put("huo",   "错误");
		resultMap.put("tu",   "或者");
		resultMap.put("total",   "漏掉");
	return resultMap;
}
}
