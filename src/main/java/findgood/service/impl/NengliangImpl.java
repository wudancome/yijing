package findgood.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import findgood.dao.rizhuMapper;
import findgood.entity.rizhu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import findgood.dao.percentMapper;
import findgood.entity.Percent;
import findgood.service.Nengliang;
import findgood.tools.GanZhi;
import findgood.tools.Yunsuan;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
public class NengliangImpl implements Nengliang{
	@Autowired
	private percentMapper percentM;
	@Autowired
	private rizhuMapper rizhuM;
	@Override
	public List<String>  Ming(String a, String b, String c, String d) throws Exception{
		List<String> baGeZi=baGeZi(a, b, c, d);
		int percentObj= GanZhi.getYueling(GanZhi.getZ(b));
		String yuefen=GanZhi.getYuefen(GanZhi.getZ(b));
		Percent nuPercent= percentM.selectById(percentObj);
		double jin=0;		double shui=0;		double mu=0;		double huo=0;		double tu=0;
		double ng=0;double nz=0;		double yg=0;double yz=0;			double rg=0;double rz=0;	double sg=0;double sz=0;
		for (int i = 0; i <baGeZi.size(); i++) {
			rizhu guaList=getRizhu(i, a, b, c, d);
			String Nzi=baGeZi.get(i);
			String wuGe= GanZhi.DFwuxing(Nzi);
			try {
				switch (wuGe) {
				case "金":jin+=GanZhi.firstNengliang(Nzi, nuPercent); break;
				case "水":shui+=GanZhi.firstNengliang(Nzi, nuPercent); break;
				case "木":mu+=GanZhi.firstNengliang(Nzi, nuPercent); break;
				case "火":huo+=GanZhi.firstNengliang(Nzi, nuPercent); break;
				case "土":tu+=GanZhi.firstNengliang(Nzi, nuPercent); break;
				default:
					break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			String zi= GanZhi.DFganzhi(i);
			switch (zi) {
			case "年干":ng=GanZhi.jutiNumber(yuefen,Nzi, nuPercent,guaList); break;
			case "年支":nz=GanZhi.jutiNumber(yuefen,Nzi, nuPercent,guaList); break;
			case "月干":yg=GanZhi.jutiNumber(yuefen,Nzi, nuPercent,guaList); break;
			case "月支":yz=GanZhi.jutiNumber(yuefen,Nzi, nuPercent,guaList); break;
			case "日干":rg=GanZhi.jutiNumber(yuefen,Nzi, nuPercent,guaList); break;
			case "日支":rz=GanZhi.jutiNumber(yuefen,Nzi, nuPercent,guaList); break;
			case "时干":sg=GanZhi.jutiNumber(yuefen,Nzi, nuPercent,guaList); break;
			case "时支":sz=GanZhi.jutiNumber(yuefen,Nzi, nuPercent,guaList); break;
			default:
				break;
			}
		}
		double total=jin+shui+mu+huo+tu;
		List<String> twoNumber=GanZhi.formatTwo(jin, shui, mu, huo, tu, total);
		List<Double> eNumber=GanZhi.getEightNumber(ng, nz, yg, yz, rg, rz, sg, sz);
		for (int i = 0; i <eNumber.size(); i++) {
			String zhiStr="";
			switch (i) {
			case 0:zhiStr=GanZhi.getG(a); break;
			case 1:zhiStr=GanZhi.getZ(a); break;
			case 2:zhiStr=GanZhi.getG(b); break;
			case 3:zhiStr=GanZhi.getZ(b); break;
			case 4:zhiStr=GanZhi.getG(c); break;
			case 5:zhiStr=GanZhi.getZ(c); break;
			case 6:zhiStr=GanZhi.getG(d); break;
			case 7:zhiStr=GanZhi.getZ(d); break;
			default:
				break;
			}
			System.out.print(zhiStr+":"+eNumber.get(i)+"\t");
		}
		System.out.println("--- ");
		for (int i = 0; i <twoNumber.size(); i++) {
			System.out.print(twoNumber.get(i)+"\t");
		}
		System.out.println("--- ");
		return twoNumber;
	}

	@Override
	public List<String>  Yun(String dayun, String a, String b, String c, String d) {
		List<String> ziList=shiGeZi(dayun,a, b, c, d);
		int percentObj= GanZhi.getYueling(GanZhi.getZ(dayun));
		String yuefen=GanZhi.getYuefen(GanZhi.getZ(dayun));
		Percent nuPercent= percentM.selectById(percentObj);
		double jin=0;		double shui=0;		double mu=0;		double huo=0;		double tu=0;
		for (int i = 0; i <ziList.size(); i++) {
			rizhu guaList=getRizhu(i, a, b, c, d);
			String Nzi=ziList.get(i);
			String zi= GanZhi.DFwuxing(Nzi);
			switch (zi) {
			case "金":jin+=GanZhi.jutiNumber(yuefen,Nzi, nuPercent,guaList); break;
			case "水":shui+=GanZhi.jutiNumber(yuefen,Nzi, nuPercent,guaList); break;
			case "木":mu+=GanZhi.jutiNumber(yuefen,Nzi, nuPercent,guaList); break;
			case "火":huo+=GanZhi.jutiNumber(yuefen,Nzi, nuPercent,guaList); break;
			case "土":tu+=GanZhi.jutiNumber(yuefen,Nzi, nuPercent,guaList); break;
			default:
				break;
			}
		}
		double total=jin+shui+mu+huo+tu;
		List<String> twoNumber=GanZhi.formatTwo(jin, shui, mu, huo, tu, total);
		for (int i = 0; i <twoNumber.size(); i++) {
			System.out.print(twoNumber.get(i)+"\t");
		}System.out.println("--- ");
		return twoNumber;
	}

	@Override
	public List<String> Nian(String dayun, String liunian, String a, String b, String c, String d) {
		List<String> ziList=shierGeZi(dayun, liunian, a, b, c, d);
		int percentObj= GanZhi.getYueling(GanZhi.getZ(liunian));
		Percent nuPercent= percentM.selectById(percentObj);
		String yuefen=GanZhi.getYuefen(GanZhi.getZ(liunian));
		double jin=0;		double shui=0;		double mu=0;		double huo=0;		double tu=0;
		for (int i = 0; i <ziList.size(); i++) {
			rizhu guaList=getRizhu(i, a, b, c, d);
			String Nzi=ziList.get(i);
			String zi= GanZhi.DFwuxing(Nzi);
			switch (zi) {
			case "金":jin+=GanZhi.jutiNumber(yuefen,Nzi, nuPercent,guaList); break;
			case "水":shui+=GanZhi.jutiNumber(yuefen,Nzi, nuPercent,guaList); break;
			case "木":mu+=GanZhi.jutiNumber(yuefen,Nzi, nuPercent,guaList); break;
			case "火":huo+=GanZhi.jutiNumber(yuefen,Nzi, nuPercent,guaList); break;
			case "土":tu+=GanZhi.jutiNumber(yuefen,Nzi, nuPercent,guaList); break;
			default:
				break;
			}
		}
		double total=jin+shui+mu+huo+tu;
		List<String> twoNumber=GanZhi.formatTwo(jin, shui, mu, huo, tu, total);
		System.out.print(liunian+"\t");
		for (int i = 0; i <twoNumber.size(); i++) {
			System.out.print(twoNumber.get(i)+"\t");
		}System.out.println("--- ");
		return twoNumber;
	}
public static List<String> baGeZi(String a, String b, String c, String d) {
	List<String> ziList=new ArrayList<String>();
	String Ng=GanZhi.getG(a);		String Nz=GanZhi.getZ(a);
	String Yg=GanZhi.getG(b);		String Yz=GanZhi.getZ(b);
	String Rg=GanZhi.getG(c);		String Rz=GanZhi.getZ(c);
	String Sg=GanZhi.getG(d);		String Sz=GanZhi.getZ(d);
	ziList.add(Ng);ziList.add(Nz);		ziList.add(Yg);ziList.add(Yz);		ziList.add(Rg);ziList.add(Rz);		ziList.add(Sg);ziList.add(Sz);
	return ziList;
}
public static List<String> shiGeZi(String dayun,String a, String b, String c, String d) {
	List<String> ziList=new ArrayList<String>();
	String Ng=GanZhi.getG(a);		String Nz=GanZhi.getZ(a);
	String Yg=GanZhi.getG(b);		String Yz=GanZhi.getZ(b);
	String Rg=GanZhi.getG(c);		String Rz=GanZhi.getZ(c);
	String Sg=GanZhi.getG(d);		String Sz=GanZhi.getZ(d);
	String YUNg=GanZhi.getG(dayun);	String YUNz=GanZhi.getZ(dayun);
	ziList.add(Ng);ziList.add(Nz);		ziList.add(Yg);ziList.add(Yz);		
	ziList.add(Rg);ziList.add(Rz);		ziList.add(Sg);ziList.add(Sz);
	ziList.add(YUNg);ziList.add(YUNz);
	return ziList;
}
public static List<String> shierGeZi(String dayun,String liunian,String a, String b, String c, String d) {
	List<String> ziList=new ArrayList<String>();
	String liung=GanZhi.getG(liunian);	String liunz=GanZhi.getZ(liunian);
	String YUNg=GanZhi.getG(dayun);	String YUNz=GanZhi.getZ(dayun);
	String Ng=GanZhi.getG(a);		String Nz=GanZhi.getZ(a);
	String Yg=GanZhi.getG(b);		String Yz=GanZhi.getZ(b);
	String Rg=GanZhi.getG(c);		String Rz=GanZhi.getZ(c);
	String Sg=GanZhi.getG(d);		String Sz=GanZhi.getZ(d);
	
	ziList.add(Ng);ziList.add(Nz);		ziList.add(Yg);ziList.add(Yz);	
	ziList.add(Rg);ziList.add(Rz);		ziList.add(Sg);ziList.add(Sz);
	ziList.add(YUNg);ziList.add(YUNz);
	ziList.add(liung);ziList.add(liunz);
	return ziList;
}
public  rizhu getRizhu(Integer i,String a, String b, String c, String d) {
	String GZzuhe="";
	if (i==0||i==1){
		GZzuhe=a;
	}else if (i==2||i==3){
		GZzuhe=b;
	}else if (i==4||i==5){
		GZzuhe=c;
	}else if (i==6||i==7){
		GZzuhe=d;
	}
	QueryWrapper<rizhu> qWrapper= new QueryWrapper<rizhu>();
	qWrapper.eq("rizhu", GZzuhe);
	rizhu guaList=rizhuM.selectOne(qWrapper);
	return guaList;
}

@Override
public Map<String, Object> first(Integer leixing,String dayun, String liunian, String a, String b, String c, String d) throws Exception {
	log.info("1。基本的能量计算");
	Map<String,Object> resultMap=new HashMap<String, Object>();
		List<String> GanZi=null;
		int percentObj= 1;
			switch (leixing) {
			case 8:	GanZi=baGeZi(a, b, c, d);break;
			case 10:GanZi=shiGeZi(dayun,a, b, c, d);break;
			case 12:GanZi=shierGeZi(dayun, liunian, a, b, c, d);break;
			default:
				break;
			}
			switch (leixing) {
			case 8:	percentObj= GanZhi.getYueling(GanZhi.getZ(b));break;
			case 10:percentObj= GanZhi.getYueling(GanZhi.getZ(dayun));break;
			case 12:percentObj= GanZhi.getYueling(GanZhi.getZ(liunian));break;
			default:
				break;
			}
		Percent nuPercent= percentM.selectById(percentObj);
	double lg=0;double lz=0;double dg=0;double dz=0;
	double ng=0;double nz=0;		double yg=0;double yz=0;			double rg=0;double rz=0;	double sg=0;double sz=0;
	for (int i = 0; i <GanZi.size(); i++) {
		String zi=GanZi.get(i);
		String Ga= GanZhi.DFganzhi(i);
		switch (Ga) {
		case "年干":ng=GanZhi.firstNengliang(zi, nuPercent); break;
		case "年支":nz=GanZhi.firstNengliang(zi, nuPercent); break;
		case "月干":yg=GanZhi.firstNengliang(zi, nuPercent); break;
		case "月支":yz=GanZhi.firstNengliang(zi, nuPercent); break;
		case "日干":rg=GanZhi.firstNengliang(zi, nuPercent); break;
		case "日支":rz=GanZhi.firstNengliang(zi, nuPercent); break;
		case "时干":sg=GanZhi.firstNengliang(zi, nuPercent); break;
		case "时支":sz=GanZhi.firstNengliang(zi, nuPercent); break;
		case "大干":dg=GanZhi.firstNengliang(zi, nuPercent); break;
		case "大支":dz=GanZhi.firstNengliang(zi, nuPercent); break;
		case "流干":lg=GanZhi.firstNengliang(zi, nuPercent); break;
		case "流支":lz=GanZhi.firstNengliang(zi, nuPercent); break;
		default:
			break;
		}
	}
	List<Double> baziD=null;
	switch (leixing) {
	case 8:	baziD=GanZhi.getEightNumber(ng, nz, yg, yz, rg, rz, sg, sz);break;
	case 10:baziD=GanZhi.getTenNumber(dg,dz,ng, nz, yg, yz, rg, rz, sg, sz);break;
	case 12:baziD=GanZhi.getTwleveNumber(lg, lz,dg, dz,ng, nz, yg, yz, rg, rz, sg, sz);break;
	default:
		break;
	}
	resultMap.put("Number",baziD);
	resultMap.put("zi",GanZi);
	resultMap.put("leixing",leixing);
	return resultMap;
}

@Override
public Map<String, Object> second(Map<String,Object> secondResult) {
	log.info("2。天干的合冲关系，及能量计算");
	Map<String,Object> resultMap=new HashMap<String, Object>();
	List<String> GanZi=(List<String>) secondResult.get("zi");
	List<Double> baziD=(List<Double>) secondResult.get("Number");
	Integer leixing= (Integer) secondResult.get("leixing");
		List<String> tianGanZi=Yunsuan.getSecondGan(GanZi, leixing);
		List<Double> tianGanNumber=Yunsuan.getSGanNum(baziD, leixing);
		Map<String,Double> bianHua=new HashMap<String, Double>();
		String Relation="";
		for (int i =0; i <tianGanZi.size(); i++){
			for (int j =i+1; j <tianGanZi.size(); j++) {
				int a=99;
				int b=99;
				Relation=tianGanZi.get(i)+tianGanZi.get(j);
				//判断是否有合冲，然后增减能量
				String heChong=Yunsuan.TianGanRelation(Relation);
				if (heChong.equals("合")) {
					a= tianGanZi.indexOf(tianGanZi.get(i));
					b= tianGanZi.indexOf(tianGanZi.get(j));
					log.info(Relation+" ： "+heChong);
					//增减的条件不知道；
					if (heChong.equals("合成")) {
						if (a==2) {
							bianHua.put("s", tianGanNumber.get(i)+5.0);
						}
					}
				}else if (heChong.equals("冲")) {
					a= tianGanZi.indexOf(tianGanZi.get(i));
					b= tianGanZi.indexOf(tianGanZi.get(j));
					log.info(Relation+" ： "+heChong);
				}
			}
		}
	resultMap.put("Number",baziD);
	resultMap.put("zi",GanZi);
	resultMap.put("leixing",leixing);
	return resultMap;
}

@Override
public Map<String, Object> third(Map<String,Object> secondResult) {
	log.info("3。干支结构的能量计算");
	Map<String,Object> resultMap=new HashMap<String, Object>();
	List<String> GanZi=(List<String>) secondResult.get("zi");
	List<Double> baziD=(List<Double>) secondResult.get("Number");
	Integer leixing= (Integer) secondResult.get("leixing");
		for (int i = 0; i <leixing; i+=2) {
			String GZzuhe=GanZi.get(i)+GanZi.get(i+1);
			QueryWrapper<rizhu> qWrapper= new QueryWrapper<rizhu>();
			qWrapper.eq("rizhu", GZzuhe);
			rizhu RelaObj=rizhuM.selectOne(qWrapper);
			log.info(GZzuhe+"  干支结构： "+RelaObj.getRelation());
			//增减的条件模糊，不清楚
		}
	resultMap.put("Number",baziD);
	resultMap.put("zi",GanZi);
	resultMap.put("leixing",leixing);
	return resultMap;
}

@Override
public Map<String, Object> four(Map<String, Object> thirdResult) {
	log.info("4。地支的三会三合六合，刑冲合害能量计算");
	Map<String,Object> resultMap=new HashMap<String, Object>();
	List<String> GanZi=(List<String>) thirdResult.get("zi");
	List<Double> baziD=(List<Double>) thirdResult.get("Number");
	Integer leixing= (Integer) thirdResult.get("leixing");
	List<String> diZhi=Yunsuan.getSecondGan(GanZi, leixing);
		for (int i =0; i <diZhi.size(); i++) {
			for (int j =i+1; j <diZhi.size(); j++) {
				for (int j2 =j+1; j2 <diZhi.size(); j2++) {
					System.out.println(i+"*"+j+"*"+j2);
					String Relation=diZhi.get(i)+diZhi.get(j)+diZhi.get(j2);
					String sanHuiHe=Yunsuan.diZhiSanHuiHe(Relation);
				}
			}
		}
	
	
	resultMap.put("Number",baziD);
	resultMap.put("zi",GanZi);
	resultMap.put("leixing",leixing);
	log.info(resultMap.get("leixing").toString());
	log.info(resultMap.get("zi").toString());
	log.info(resultMap.get("Number").toString());
	return resultMap;
}
}
