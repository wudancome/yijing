package findgood.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import findgood.dao.ganzhiMapper;
import findgood.dao.nqyMapper;
import findgood.dao.peopleMapper;
import findgood.dao.rizhuMapper;
import findgood.dao.rqsMapper;
import findgood.entity.ganzhi;
import findgood.entity.nqy;
import findgood.entity.people;
import findgood.entity.rizhu;
import findgood.entity.rqs;
import findgood.service.Bazi;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
public class BaziImpl implements Bazi {
	@Autowired
	private nqyMapper nqy;
	@Autowired
	private rqsMapper rqs;
	@Autowired
	private peopleMapper people;
	@Autowired
	private ganzhiMapper gM;
	@Autowired
	private rizhuMapper rizhuMap;
	@Override
	public int savenqy(nqy a) {
		int result = nqy.insert(a);
		log.info("保存成功！");
		return result;
	}

	@Override
	public int saverqs(rqs a) {
		int result = rqs.insert(a);
		log.info("保存成功！");
		return result;
	}

	@Override
	public int savepeople(people a) {
		int result = people.insert(a);
		log.info("保存成功！");
		return result;
	}

	@Override
	public String selectTiangan(Integer a) {
		ganzhi dizhi = gM.selectById(a);
//		log.info("地支："+dizhi.getTiangan());
		return dizhi.getTiangan();
	}

	@Override
	public String selectDizhi(Integer a) {
		ganzhi gan = gM.selectById(a);
//		log.info("天干："+gan.getDizhi());
		return gan.getDizhi();
	}

	@Override
	public String selectYuezhu(Integer yuefen, Integer nianfen) {
		nqy c = nqy.selectById(yuefen);
		return getYuezhu(nianfen, c);
	}

	@Override
	public String selectShizhu(String ri, Integer shi) {
		rqs rqsbean=rqs.selectById(shi);
		return getShizhu(ri.substring(0,1), rqsbean);
	}

	public String getYuezhu(Integer nianfen, nqy c) {
		String yuezhu = "空的";
		if (nianfen == 0 || nianfen == 5) {
			yuezhu = c.getJiaji();
		} else if (nianfen == 1 || nianfen == 6) {
			yuezhu = c.getYigeng();
		} else if (nianfen == 2 || nianfen == 7) {
			yuezhu = c.getBingxin();
		} else if (nianfen == 3 || nianfen == 8) {
			yuezhu = c.getDingren();
		} else if (nianfen == 4 || nianfen == 9) {
			yuezhu = c.getWugui();
		}
		log.info("月柱干支："+yuezhu);
		return yuezhu;
	}
	public String getShizhu(String ri, rqs c) {
		String shigan = "空的";
		if (ri.equals("甲")|| ri.equals("己")) {
			shigan = c.getJiaji();
		} else if (ri.equals("乙") || ri.equals("庚")) {
			shigan = c.getYigeng();
		} else if (ri.equals("丙") || ri.equals("辛")) {
			shigan = c.getBingxin();
		} else if (ri.equals("丁") || ri.equals("壬")) {
			shigan = c.getDingren();
		} else if (ri.equals("戊") || ri.equals("癸")) {
			shigan = c.getWugui();
		}
		log.info("时柱干支："+shigan);
		return shigan;
	}

	@Override
	public String selectRizhu(Integer a) {
		rizhu rizhu= rizhuMap.selectById(a);
		return rizhu.getRizhu();
	}
	
}
