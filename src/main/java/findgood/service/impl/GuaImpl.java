package findgood.service.impl;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import findgood.dao.GuaciMapper;
import findgood.dao.YaociMapper;
import findgood.dao.percentMapper;
import findgood.entity.Guaci;
import findgood.entity.Percent;
import findgood.entity.Yaoci;
import findgood.service.Gua;
import findgood.tools.ExcelUtil;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
public class GuaImpl implements Gua{
@Autowired
private GuaciMapper Gmapper;
@Autowired
private YaociMapper Ymapper;
@Autowired
private	percentMapper Percent;
	@Override
	public Map<String, Object> selceGuaci(String miaoshu) {
		Map<String,Object> resultMap=new HashMap<String, Object>();
		List<String> gua=new ArrayList<String>();		List<String> yi=new ArrayList<String>();		List<String> er=new ArrayList<String>();		List<String> san=new ArrayList<String>();		List<String> si=new ArrayList<String>();		List<String> wu=new ArrayList<String>();		List<String> liu=new ArrayList<String>();
		if (miaoshu.equals("")) {
			miaoshu="元亨利贞";
		}
			QueryWrapper<Guaci> qWrapper=new QueryWrapper<Guaci>();
			qWrapper.like(true, "gfanyi", miaoshu);
			List<Guaci> guaList=Gmapper.selectList(qWrapper);
			int yaoid=65;
			String guaNumber="";
			String nowGua="";
			if (guaList.size()>0) {
				int numb = guaList.size();
				Random ran = new Random();
				int n11 = ran.nextInt(numb);
				n11 = Math.abs(ran.nextInt() % numb);
				Guaci suiji = guaList.get(n11);
				
				yaoid= suiji.getId();
				nowGua=suiji.getGname();
				gua.add(suiji.getGname());
				gua.add(suiji.getGci());
				gua.add(suiji.getGfanyi());
				resultMap.put("xuhao",suiji.getId());
				for (int i = 0; i <guaList.size(); i++) {
					guaNumber+="["+guaList.get(i).getId()+guaList.get(i).getGname()+"]\t";
				}
			}else {
				gua.add("空");
				gua.add("空");
				gua.add("空");
			}
			
			log.info("当前卦为["+nowGua+"],与["+miaoshu+"]\t相关的卦有["+guaList.size()+"]个\t是第"+guaNumber);
			QueryWrapper<Yaoci> Wrapper=new QueryWrapper<Yaoci>();
			Wrapper.eq("gid", yaoid);
			List<Yaoci> yaoList=Ymapper.selectList(Wrapper);
			if (yaoList.size()>0) {
				yi.add(yaoList.get(0).getYname());				yi.add(yaoList.get(0).getYci());				yi.add(yaoList.get(0).getYfanyi());				yi.add(yaoList.get(0).getWuyifan());				
				er.add(yaoList.get(1).getYname());				er.add(yaoList.get(1).getYci());				er.add(yaoList.get(1).getYfanyi());				er.add(yaoList.get(1).getWuyifan());				
				san.add(yaoList.get(2).getYname());				san.add(yaoList.get(2).getYci());				san.add(yaoList.get(2).getYfanyi());				san.add(yaoList.get(2).getWuyifan());				
				si.add(yaoList.get(3).getYname());				si.add(yaoList.get(3).getYci());				si.add(yaoList.get(3).getYfanyi());				si.add(yaoList.get(3).getWuyifan());
				wu.add(yaoList.get(4).getYname());				wu.add(yaoList.get(4).getYci());				wu.add(yaoList.get(4).getYfanyi());				wu.add(yaoList.get(4).getWuyifan());				
				liu.add(yaoList.get(5).getYname());				liu.add(yaoList.get(5).getYci());				liu.add(yaoList.get(5).getYfanyi());				liu.add(yaoList.get(5).getWuyifan());
			}else {
				yi.add("空");				yi.add("空");				yi.add("空");				yi.add("空");				
				er.add("空");				er.add("空");				er.add("空");				er.add("空");				
				san.add("空");				san.add("空");				san.add("空");				san.add("空");				
				si.add("空");				si.add("空");				si.add("空");				si.add("空");				
				wu.add("空");				wu.add("空");				wu.add("空");				wu.add("空");				
				liu.add("空");				liu.add("空");				liu.add("空");				liu.add("空");
			}
		resultMap.put("yi", yi);
		resultMap.put("gua", gua);
		resultMap.put("er", er);
		resultMap.put("san", san);
		resultMap.put("si", si);
		resultMap.put("wu", wu);
		resultMap.put("liu", liu);
		resultMap.put("total", guaList.size());
		return resultMap;
	}
	@Override
	public int insertGua(Guaci oneGua) {
		int result= Gmapper.insert(oneGua);
		return result;
	}
	@Override
	public int insertYao(Yaoci oneYao) {
		int result= Ymapper.insert(oneYao);
		return result;
	}
	@Override
	public int insertNengLiang(Percent percent) {
		int result= Percent.insert(percent);
		return result;
	}
	@Override
	public int fileSaveGua(MultipartFile file) {
		try {
			List<Guaci> guaciList=ExcelUtil.Fgua(new BufferedInputStream(file.getInputStream()));
			for (int i = 0; i <guaciList.size(); i++){
				insertGua(guaciList.get(i));
			}
		log.info("保存卦辞成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 99;
	}

	@Override
	public int fileSaveYao(MultipartFile file) {
		try {
			List<Yaoci> yaociList=ExcelUtil.Fyao(new BufferedInputStream(file.getInputStream()));
			for (int i = 0; i <yaociList.size(); i++){
				insertYao(yaociList.get(i));
			}
		log.info("保存爻辞成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	@Override
	public Map<String, Object> chooseGuaorder(int Number) {
		Map<String,Object> resultMap=new HashMap<String, Object>();
		List<String> gua=new ArrayList<String>();		List<String> yi=new ArrayList<String>();		List<String> er=new ArrayList<String>();		List<String> san=new ArrayList<String>();		List<String> si=new ArrayList<String>();		List<String> wu=new ArrayList<String>();		List<String> liu=new ArrayList<String>();
			QueryWrapper<Guaci> qWrapper=new QueryWrapper<Guaci>();
			qWrapper.eq("id", Number);
			List<Guaci> guaList=Gmapper.selectList(qWrapper);
			int yaoid=65;
			String guaNumber="";
			String nowGua="";
			if (guaList.size()>0) {
				int numb = guaList.size();
				Random ran = new Random();
				int n11 = ran.nextInt(numb);
				n11 = Math.abs(ran.nextInt() % numb);
				Guaci suiji = guaList.get(n11);
				
				yaoid= suiji.getId();
				nowGua=suiji.getGname();
				gua.add(suiji.getGname());
				gua.add(suiji.getGci());
				gua.add(suiji.getGfanyi());
				for (int i = 0; i <guaList.size(); i++) {
					guaNumber+="["+guaList.get(i).getId()+guaList.get(i).getGname()+"]\t";
				}
			}else {
				gua.add("空");
				gua.add("空");
				gua.add("空");
			}
			log.info("当前卦为第["+Number+"]卦");
			QueryWrapper<Yaoci> Wrapper=new QueryWrapper<Yaoci>();
			Wrapper.eq("gid", yaoid);
			List<Yaoci> yaoList=Ymapper.selectList(Wrapper);
			if (yaoList.size()>0) {
				yi.add(yaoList.get(0).getYname());				yi.add(yaoList.get(0).getYci());				yi.add(yaoList.get(0).getYfanyi());				yi.add(yaoList.get(0).getWuyifan());				
				er.add(yaoList.get(1).getYname());				er.add(yaoList.get(1).getYci());				er.add(yaoList.get(1).getYfanyi());				er.add(yaoList.get(1).getWuyifan());				
				san.add(yaoList.get(2).getYname());				san.add(yaoList.get(2).getYci());				san.add(yaoList.get(2).getYfanyi());				san.add(yaoList.get(2).getWuyifan());				
				si.add(yaoList.get(3).getYname());				si.add(yaoList.get(3).getYci());				si.add(yaoList.get(3).getYfanyi());				si.add(yaoList.get(3).getWuyifan());
				wu.add(yaoList.get(4).getYname());				wu.add(yaoList.get(4).getYci());				wu.add(yaoList.get(4).getYfanyi());				wu.add(yaoList.get(4).getWuyifan());				
				liu.add(yaoList.get(5).getYname());				liu.add(yaoList.get(5).getYci());				liu.add(yaoList.get(5).getYfanyi());				liu.add(yaoList.get(5).getWuyifan());
			}else {
				yi.add("空");				yi.add("空");				yi.add("空");				yi.add("空");				
				er.add("空");				er.add("空");				er.add("空");				er.add("空");				
				san.add("空");				san.add("空");				san.add("空");				san.add("空");				
				si.add("空");				si.add("空");				si.add("空");				si.add("空");				
				wu.add("空");				wu.add("空");				wu.add("空");				wu.add("空");				
				liu.add("空");				liu.add("空");				liu.add("空");				liu.add("空");
			}
		resultMap.put("yi", yi);
		resultMap.put("gua", gua);
		resultMap.put("er", er);
		resultMap.put("san", san);
		resultMap.put("si", si);
		resultMap.put("wu", wu);
		resultMap.put("liu", liu);
		resultMap.put("total", guaList.size());
		resultMap.put("xuhao", guaList.get(0).getId());
		return resultMap;
	}
	@Override
	public Map<String, Object> chooseGuaname(String name) {
		Map<String,Object> resultMap=new HashMap<String, Object>();
		List<String> gua=new ArrayList<String>();		List<String> yi=new ArrayList<String>();		List<String> er=new ArrayList<String>();		List<String> san=new ArrayList<String>();		List<String> si=new ArrayList<String>();		List<String> wu=new ArrayList<String>();		List<String> liu=new ArrayList<String>();
			QueryWrapper<Guaci> qWrapper=new QueryWrapper<Guaci>();
			qWrapper.eq("gname", name);
			List<Guaci> guaList=Gmapper.selectList(qWrapper);
			int yaoid=65;
			String guaNumber="";
			String nowGua="";
			if (guaList.size()>0) {
				int numb = guaList.size();
				Random ran = new Random();
				int n11 = ran.nextInt(numb);
				n11 = Math.abs(ran.nextInt() % numb);
				Guaci suiji = guaList.get(n11);
				
				yaoid= suiji.getId();
				nowGua=suiji.getGname();
				gua.add(suiji.getGname());
				gua.add(suiji.getGci());
				gua.add(suiji.getGfanyi());
				for (int i = 0; i <guaList.size(); i++) {
					guaNumber+="["+guaList.get(i).getId()+guaList.get(i).getGname()+"]\t";
				}
			}else {
				gua.add("空");
				gua.add("空");
				gua.add("空");
			}
			log.info("当前卦为第"+guaNumber+"]卦");
			QueryWrapper<Yaoci> Wrapper=new QueryWrapper<Yaoci>();
			Wrapper.eq("gid", yaoid);
			List<Yaoci> yaoList=Ymapper.selectList(Wrapper);
			if (yaoList.size()>0) {
				yi.add(yaoList.get(0).getYname());				yi.add(yaoList.get(0).getYci());				yi.add(yaoList.get(0).getYfanyi());				yi.add(yaoList.get(0).getWuyifan());				
				er.add(yaoList.get(1).getYname());				er.add(yaoList.get(1).getYci());				er.add(yaoList.get(1).getYfanyi());				er.add(yaoList.get(1).getWuyifan());				
				san.add(yaoList.get(2).getYname());				san.add(yaoList.get(2).getYci());				san.add(yaoList.get(2).getYfanyi());				san.add(yaoList.get(2).getWuyifan());				
				si.add(yaoList.get(3).getYname());				si.add(yaoList.get(3).getYci());				si.add(yaoList.get(3).getYfanyi());				si.add(yaoList.get(3).getWuyifan());
				wu.add(yaoList.get(4).getYname());				wu.add(yaoList.get(4).getYci());				wu.add(yaoList.get(4).getYfanyi());				wu.add(yaoList.get(4).getWuyifan());				
				liu.add(yaoList.get(5).getYname());				liu.add(yaoList.get(5).getYci());				liu.add(yaoList.get(5).getYfanyi());				liu.add(yaoList.get(5).getWuyifan());
			}else {
				yi.add("空");				yi.add("空");				yi.add("空");				yi.add("空");				
				er.add("空");				er.add("空");				er.add("空");				er.add("空");				
				san.add("空");				san.add("空");				san.add("空");				san.add("空");				
				si.add("空");				si.add("空");				si.add("空");				si.add("空");				
				wu.add("空");				wu.add("空");				wu.add("空");				wu.add("空");				
				liu.add("空");				liu.add("空");				liu.add("空");				liu.add("空");
			}
		resultMap.put("yi", yi);
		resultMap.put("gua", gua);
		resultMap.put("er", er);
		resultMap.put("san", san);
		resultMap.put("si", si);
		resultMap.put("wu", wu);
		resultMap.put("liu", liu);
		resultMap.put("total", guaList.size());
		resultMap.put("xuhao", guaList.get(0).getId());
		return resultMap;
	}
	@Override
	public int fileSaveNengLiang(MultipartFile file) {
		try {
			List<Percent> nengLiangList=ExcelUtil.Fnengliang(new BufferedInputStream(file.getInputStream()));
			for (int i = 0; i <nengLiangList.size(); i++){
				insertNengLiang(nengLiangList.get(i));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return 0;
	}
}
