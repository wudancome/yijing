package findgood.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import findgood.dao.cangganMapper;
import findgood.dao.shishenMapper;
import findgood.dao.thshenMapper;
import findgood.entity.canggan;
import findgood.entity.shishen;
import findgood.entity.thshen;
import findgood.service.Yongshen;
import findgood.tools.GanZhi;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
public class YongshenImpl implements Yongshen {
	@Autowired
	private shishenMapper shishenM;
	@Autowired
	private cangganMapper cangM;
	@Autowired
	private thshenMapper thshenM;
	@Override
	public String selectshen(Integer rizhu, String tiangan) {
		shishen s = shishenM.selectById(rizhu);
		String shen = "空的";
		shen=GanZhi.getShen(tiangan, s);
		return shen;
	}
	@Override
	public String seleCanggan(Integer a) {
		canggan canggan= cangM.selectById(a);
		return canggan.getCang();
	}
	@Override
	public String selethshen(Integer yuefen, String rizhutiangan) {
		thshen ss= thshenM.selectById(yuefen);
		String shen = "空的";
		shen=GanZhi.getthshen(rizhutiangan,ss);
		return shen;
	}
}
