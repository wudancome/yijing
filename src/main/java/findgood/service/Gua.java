package findgood.service;

import java.util.Map;
import org.springframework.web.multipart.MultipartFile;
import findgood.entity.Guaci;
import findgood.entity.Percent;
import findgood.entity.Yaoci;

public interface Gua {
	Map<String, Object> selceGuaci(String miaoshu);
	Map<String, Object> chooseGuaname(String miaoshu);
	Map<String, Object> chooseGuaorder(int guaNumber);
	int insertGua(Guaci oneGua);
	int insertYao(Yaoci oneYao);
	int insertNengLiang(Percent percent);
	int fileSaveGua(MultipartFile file);
	int fileSaveYao(MultipartFile file);
	int fileSaveNengLiang(MultipartFile file);
}
