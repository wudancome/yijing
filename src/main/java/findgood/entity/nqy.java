package findgood.entity;

import org.springframework.stereotype.Component;
import lombok.Data;
import lombok.NoArgsConstructor;
@Component
@Data
@NoArgsConstructor
public class nqy {//年起月
	private Integer id;//序号
	private String yuefen;//月份
	private String jiaji;//甲己
	private String yigeng;//乙庚
	private String bingxin;//丙辛
	private String dingren;//丁壬
	private String wugui;//戊癸
}
