package findgood.entity;

import org.springframework.stereotype.Component;
import lombok.Data;
import lombok.NoArgsConstructor;
@Component
@Data
@NoArgsConstructor
public class rqs {//月起时
	private Integer id;//序号
	private String shichen;//时辰
	private String jiaji;//甲己
	private String yigeng;//乙庚
	private String bingxin;//丙辛
	private String dingren;//丁壬
	private String wugui;//戊癸
}
