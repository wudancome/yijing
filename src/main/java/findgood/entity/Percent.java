package findgood.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Percent {
	private Integer id;//地支的序号，从1-12，寅到丑；
	
	private Integer jia;
	private Integer yi;
	private Integer bing;
	private Integer ding;
	private Integer wu;
	private Integer ji;
	private Integer geng;
	private Integer xin;
	private Integer ren;
	private Integer gui;
	
	private Integer yin;
	private Integer mao;
	private Integer chen;
	private Integer si;
	private Integer wuu;
	private Integer wei;
	private Integer shen;
	private Integer you;
	private Integer xu;
	private Integer hai;
	private Integer zi;
	private Integer chou;
}
