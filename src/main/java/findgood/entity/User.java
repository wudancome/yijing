package findgood.entity;

import java.io.Serializable;
import java.util.Date;
import org.springframework.stereotype.Component;
import lombok.Data;
@Data
@Component
public class User implements Serializable{
    private Integer id;
    private String userName;
    private String passWord;
    private Integer sex;
    private Date birthday;
	public  User() {
		
	}
	public User(Integer id,String userName,String passWord,Integer sex,Date birthday) {
		this.id=id;
		this.userName=userName;
		this.passWord=passWord;
		this.sex=sex;
		this.birthday=birthday;
	}
}
