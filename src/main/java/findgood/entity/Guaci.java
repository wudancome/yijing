package findgood.entity;

import org.springframework.stereotype.Component;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@Component
public class Guaci {
	private Integer id;//序号
	private String gname;//卦名
	private String gci;//卦辞
	private String gfanyi;//卦辞解释
}
