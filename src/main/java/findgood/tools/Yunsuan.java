package findgood.tools;

import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public class Yunsuan {
	//根据类型，获得天干的字
	public static List<String> getSecondGan(List<String> wuList,Integer leixing){
		List<String> tianGan=new ArrayList<String>();
		if (leixing==8) {
			tianGan.add(wuList.get(0));
			tianGan.add(wuList.get(2));
			tianGan.add(wuList.get(4));
			tianGan.add(wuList.get(6));
		}else if (leixing==10) {
			tianGan.add(wuList.get(0));
			tianGan.add(wuList.get(2));
			tianGan.add(wuList.get(4));
			tianGan.add(wuList.get(6));
			tianGan.add(wuList.get(8));
		}else if (leixing==12) {
			tianGan.add(wuList.get(0));
			tianGan.add(wuList.get(2));
			tianGan.add(wuList.get(4));
			tianGan.add(wuList.get(6));
			tianGan.add(wuList.get(8));
			tianGan.add(wuList.get(10));
		}
			return tianGan;
	}
public static List<Double> getSGanNum(List<Double> wuList,Integer leixing) {
	List<Double> tianGan=new ArrayList<Double>();
	if (leixing==8) {
		tianGan.add(wuList.get(0));
		tianGan.add(wuList.get(2));
		tianGan.add(wuList.get(4));
		tianGan.add(wuList.get(6));
	}else if (leixing==10) {
		tianGan.add(wuList.get(0));
		tianGan.add(wuList.get(2));
		tianGan.add(wuList.get(4));
		tianGan.add(wuList.get(6));
		tianGan.add(wuList.get(8));
	}else if (leixing==12) {
		tianGan.add(wuList.get(0));
		tianGan.add(wuList.get(2));
		tianGan.add(wuList.get(4));
		tianGan.add(wuList.get(6));
		tianGan.add(wuList.get(8));
		tianGan.add(wuList.get(10));
	}
	return tianGan;
}
public static String TianGanRelation(String gan) {
	if (gan.equals("甲庚")||gan.equals("庚甲")) {
		return "冲";
	} else if (gan.equals("乙辛")||gan.equals("辛乙")) {
		return "冲";
	} else if (gan.equals("丙壬")||gan.equals("壬丙")) {
		return "冲";
	} else if (gan.equals("丁癸")||gan.equals("癸丁")) {
		return "冲";
	} else if (gan.equals("甲己")||gan.equals("己甲")) {
		return "合";
	} else if (gan.equals("乙庚")||gan.equals("庚乙")) {
		return "合";
	} else if (gan.equals("丙辛")||gan.equals("辛丙")) {
		return "合";
	} else if (gan.equals("丁壬")||gan.equals("壬丁")) {
		return "合";
	} else if (gan.equals("戊癸")||gan.equals("癸戊")) {
		return "合";
	}
	return "无";
}
public static String diZhiSanHuiHe(String gan) {
	if (gan.equals("亥子丑")||gan.equals("亥丑子")||gan.equals("子丑亥")||gan.equals("子亥丑")||gan.equals("丑子亥")||gan.equals("丑亥子")) {
		return "三会";
	} else if (gan.equals("寅卯辰")||gan.equals("寅辰卯")||gan.equals("卯辰寅")||gan.equals("卯寅辰")||gan.equals("辰寅卯")||gan.equals("辰卯寅")) {
		return "三会";
	} else if (gan.equals("巳午未")||gan.equals("巳未午")||gan.equals("午未巳")||gan.equals("午巳未")||gan.equals("未巳午")||gan.equals("未午巳")) {
		return "三会";
	} else if (gan.equals("申酉戌")||gan.equals("申戌酉")||gan.equals("酉申戌")||gan.equals("酉戌申")||gan.equals("戌申酉")||gan.equals("戌酉申")) {
		return "三会";
	} else if (gan.equals("申子辰")||gan.equals("申子辰")||gan.equals("子辰申")||gan.equals("子申辰")||gan.equals("辰申子")||gan.equals("辰子申")) {
		return "三合";
	} else if (gan.equals("寅午戌")||gan.equals("寅午戌")||gan.equals("寅午戌")||gan.equals("寅午戌")||gan.equals("寅午戌")||gan.equals("寅午戌")) {
		return "三合";
	} else if (gan.equals("亥卯未")||gan.equals("辛丙")) {
		return "三合";
	} else if (gan.equals("巳酉丑")||gan.equals("壬丁")) {
		return "三合";
	}
	return "无";
}

}
