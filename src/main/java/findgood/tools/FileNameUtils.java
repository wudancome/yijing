package findgood.tools;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *	生成文件名的类
 * @author 唐春
 * @version 1.0
 * @since JDK1.8
 */
public class FileNameUtils {

	/**
	 * 获取文件后缀
	 * @param fileName 参数
	 * @return String 返回字符串
	 */
	public static String getSuffix(String fileName) {
		return fileName.substring(fileName.lastIndexOf("."));
	}
	public static String getgoodPaizi(String a){
		String daitihaun =a.replace(" ", "");
		daitihaun=daitihaun.replace("'", "");
		String paiziNo=daitihaun;
		String paiziAll=daitihaun;
		String reg = "[^\u4e00-\u9fa5]";  
		paiziAll = paiziAll.replaceAll(reg, "");
//		log.info("4全中文："+paiziAll);
		String paizi ="测试字符";
		if (paiziAll.matches("[\u4E00-\u9FA5]+")) {
			paizi =paiziAll;
//			paizi =paiziAll.substring(0,1);
		} else {
			paizi =paiziNo;
//			paizi =paiziNo.substring(0,1);
		}
//		log.info("5这个字：" + paizi);
		return paizi;
	}
	public static String getgoodName(String a){
		String daitihaun =a.replace(" ", "");
		daitihaun=daitihaun.replace("'", "");
//		log.info("5这个字：" + daitihaun);
		return daitihaun;
	}
	public static String getDIYid(String geshi) {
		Date aDate = new Date();
		SimpleDateFormat DateFormat = new SimpleDateFormat(geshi);
		String DIYid = DateFormat.format(aDate);
		return DIYid;
	}
	public static Integer getDIYint(String geshi) throws InterruptedException {
		Date aDate = new Date();
		SimpleDateFormat DateFormat = new SimpleDateFormat(geshi);
		String DIYid = DateFormat.format(aDate);
		Integer aa=Integer.valueOf(DIYid);
		return aa;
	}
	public static Date getDIYdate() {
		Date aDate = new Date();
		return aDate;
	}
	public static String getidName(){
		Date aDate = new Date();
		SimpleDateFormat DateFormat = new SimpleDateFormat("yyyyMMddHHmm");
		return DateFormat.format(aDate);
	}
	public static String getgoodName(){
		Date aDate = new Date();
		SimpleDateFormat DateFormat = new SimpleDateFormat("yyyyMMddHHmmSSSS");
		return DateFormat.format(aDate);
	}
	/**
	 * 生成新的文件名
	 * @param fileOriginName 源文件名
	 * @return String 返回字符串
	 */
	public static String getFileName(String fileOriginName) {
		
//		return UUIDUtils.getUUID() + FileNameUtils.getSuffix(fileOriginName);
		return "控溫家";
	}
	/**
	 * 	创建储存对应照片的新文件夹
	 * @return String 返回根据时间生成的文件名
	 */
	public static String createNewName(String geshi){
		Date aDate = new Date();
		SimpleDateFormat DateFormat = new SimpleDateFormat(geshi);//"YYYY"+"-"+"MM"+"-"+"dd"+"-"+"HHmm"
		String newCreate = DateFormat.format(aDate);
		try {
			//D:/mywork2/findname/src/main/resources/static/001findme/
			File wenjian = new File("D:/001findme/"+newCreate);
			File shangjiFile=new File("D:/001findme/");
			if (!shangjiFile.exists()) {
				shangjiFile.mkdir();
				wenjian.mkdir();
			}else if (!wenjian.exists()) {
				wenjian.mkdir();
				System.out.println("创建文件"+newCreate+"成功");
			}else {
				System.out.println("文件夹"+newCreate+"img已经存在");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newCreate;
	}
}
