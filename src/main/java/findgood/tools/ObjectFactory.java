package findgood.tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 	静态工厂方法，生产集合对象
 * @author 唐春
 * @version 1.0 2021年8月25日 下午5:22:26 
 * @since JDK 1.8
 *
 */
public class ObjectFactory {
	public static ArrayList<Map<String, String>> getListMap() {
		return new ArrayList<Map<String, String>>();
	}
	public static ArrayList<String> getArrayList() {
		return new ArrayList<String>();
	}
	public static ArrayList<List<String>> getArrListlist() {
		return new ArrayList<List<String>>();
	}
	public static HashMap<String, String> getHashMap() {
		return new HashMap<String, String>();
	}
}
