package findgood.tools;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
//将3560张图片的名字转换为excel表格，导出到本地F盘；
//创建身份证库，需要照片和姓名身份证数据；身份证数据通过这个类获取，姓名只有自己挨个看，然后根据导出的身份证表格；写入excel表格
public class photoToExce {
	public static void zz() {
		XSSFWorkbook workbook = new XSSFWorkbook();
	    XSSFSheet sheet1 = workbook.createSheet("sheet1");
	    List<String> photoList = new ArrayList<String>();
		ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		try {
			Resource[] resources = resolver.getResources("img/*.jpg");
			for (Resource resource : resources) {
				String filename = resource.getFilename();
				photoList.add(filename.substring(0, 18));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		//测试用
//		for (String string : photoList) {
//			System.out.println(string);
//		}
		 for (int i = 0; i <1; i++) {
		        XSSFRow row = sheet1.createRow(i);
		        row.createCell(0).setCellValue("身份证号码");
		    }
	    for (int i = 0; i <photoList.size(); i++) {
	        XSSFRow row = sheet1.createRow(i+1);
	        row.createCell(0).setCellValue(photoList.get(i));
	    }
	    try {
	    	//导出表格到本地F盘
			String filePath = FileNameUtils.getidName()+".xlsx";
			FileOutputStream fileOutputStream = new FileOutputStream(filePath);
			workbook.write(fileOutputStream);
			fileOutputStream.close();
			workbook.close();
			System.out.println("数据生成成功！");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}