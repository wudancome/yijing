package findgood.tools;

import findgood.entity.Percent;
import findgood.entity.rizhu;
import findgood.entity.shishen;
import findgood.entity.thshen;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Slf4j
public class GanZhi {
	public static void main(String[] args) {
//		for (int i = 1; i <13; i++) {
//			System.out.print(i+"\t");
//			YLgetRZ(1993,8,29);
//			YLgetRZ(1996,10,29);
//			YLgetRZ(1993,1,3);
//		}
//			for (int i =0; i <4; i++) {
//				for (int j =i+1; j <4; j++) {
//					System.out.println(i+"*"+j);
//				}
//			}
//			for (int i =0; i <4; i++) {
//				for (int j =i+1; j <4; j++) {
//					for (int j2 =j+1; j2 <4; j2++) {
//						System.out.println(i+"*"+j+"*"+j2);
//					}
//				}
//			}
			for (int i =0; i <5; i++) {
				for (int j =i+1; j <5; j++) {
					for (int j2 =j+1; j2 <5; j2++) {
						System.out.println(i+"*"+j+"*"+j2);
					}
				}
			}
	}
	public static int YLgetRZ(int nian, int yue, int ri){//阳历平年365，闰年366;
		//农历平年354，闰年355，阳历1925年2月9号，甲子日为1；起始点。但是缺少农历闰月方法。
		int oneYear=0;
		int dayNumber=0;
		for (int i =1925; i <nian; i++) {
			//只需要知道，农历的每一年有多少天；
			if (i%100==0) {
				if (i%400==0) {
					oneYear=366;
				}else {
					oneYear=365;
				}
			}else {
				if (i%4==0) {
					oneYear=366;
				}else {
					oneYear=365;
				}
			}
			dayNumber+=oneYear;
		}
		int dangDay=ri;
		for (int i =1; i <yue; i++) {
			switch (i) {
			case 1:dangDay+=31; break;
			case 2:if (nian%100==0){
				if (nian%400==0){
					dangDay+=29;
				}else {
					dangDay+=28;
				}
			} else {
				if (nian%4==0){
					dangDay+=29;
				}else {
					dangDay+=28;
				}
			} break;
			case 3:dangDay+=31; break;
			case 4:dangDay+=30; break;
			case 5:dangDay+=31; break;
			case 6:dangDay+=30; break;
			case 7:dangDay+=31; break;
			case 8:dangDay+=31; break;
			case 9:dangDay+=30; break;
			case 10:dangDay+=31; break;
			case 11:dangDay+=30; break;
			default:
				break;
			}
		}
		dayNumber=dayNumber-39+dangDay;
		int cha = dayNumber%60;
		System.out.println("60甲子序号："+cha);
		return cha;
	}
	public static int getGan(String riYuan, Integer riyuan) {
		switch (riYuan) {
		case "甲":
			riyuan = 0;
			break;
		case "乙":
			riyuan = 1;
			break;
		case "丙":
			riyuan = 2;
			break;
		case "丁":
			riyuan = 3;
			break;
		case "戊":
			riyuan = 4;
			break;
		case "己":
			riyuan = 5;
			break;
		case "庚":
			riyuan = 6;
			break;
		case "辛":
			riyuan = 7;
			break;
		case "壬":
			riyuan = 8;
			break;
		case "癸":
			riyuan = 9;
			break;
		default:
			break;
		}
		return riyuan;
	}

	public static int getYueling(String dizhi) {
		int di = 0;
		switch (dizhi) {
		case "寅":
			di = 1;
			break;
		case "卯":
			di = 2;
			break;
		case "辰":
			di = 3;
			break;
		case "巳":
			di = 4;
			break;
		case "午":
			di = 5;
			break;
		case "未":
			di = 6;
			break;
		case "申":
			di = 7;
			break;
		case "酉":
			di = 8;
			break;
		case "戌":
			di = 9;
			break;
		case "亥":
			di = 10;
			break;
		case "子":
			di = 11;
			break;
		case "丑":
			di = 12;
			break;
		default:
			break;
		}
		return di;
	}
	public static int getZhi(String dizhi) {
		int di = 0;
		switch (dizhi) {
		case "子":
			di = 0;
			break;
		case "丑":
			di = 1;
			break;
		case "寅":
			di = 2;
			break;
		case "卯":
			di = 3;
			break;
		case "辰":
			di = 4;
			break;
		case "巳":
			di = 5;
			break;
		case "午":
			di = 6;
			break;
		case "未":
			di = 7;
			break;
		case "申":
			di = 8;
			break;
		case "酉":
			di = 9;
			break;
		case "戌":
			di = 10;
			break;
		case "亥":
			di = 11;
			break;
		default:
			break;
		}
		return di;
	}
public static String getStrGan(int gan) {
	String ganStr="";
	switch (gan) {
	case 0:ganStr="癸"; break;
	case 1:ganStr="甲"; break;
	case 2:ganStr="乙"; break;
	case 3:ganStr="丙"; break;
	case 4:ganStr="丁"; break;
	case 5:ganStr="戊"; break;
	case 6:ganStr="己"; break;
	case 7:ganStr="庚"; break;
	case 8:ganStr="辛"; break;
	case 9:ganStr="壬"; break;
	default:
		break;
	}
	return ganStr;
}
public static String getStrZhi(int zhi) {
	String zhiStr="";
	switch (zhi) {
	case 1:zhiStr="子"; break;
	case 2:zhiStr="丑"; break;
	case 3:zhiStr="寅"; break;
	case 4:zhiStr="卯"; break;
	case 5:zhiStr="辰"; break;
	case 6:zhiStr="巳"; break;
	case 7:zhiStr="午"; break;
	case 8:zhiStr="未"; break;
	case 9:zhiStr="申"; break;
	case 10:zhiStr="酉"; break;
	case 11:zhiStr="戌"; break;
	case 0:zhiStr="亥"; break;
	default:
		break;
	}
	return zhiStr;
}
	public static String getShen(String tiangan, shishen s) {
		String shen = "";
		switch (tiangan) {
		case "甲":
			shen = s.getJia();
			break;
		case "乙":
			shen = s.getYi();
			break;
		case "丙":
			shen = s.getBing();
			break;
		case "丁":
			shen = s.getDing();
			break;
		case "戊":
			shen = s.getWu();
			break;
		case "己":
			shen = s.getJi();
			break;
		case "庚":
			shen = s.getGeng();
			break;
		case "辛":
			shen = s.getXin();
			break;
		case "壬":
			shen = s.getRen();
			break;
		case "癸":
			shen = s.getGui();
			break;
		default:
			break;
		}
		return shen;
	}
	public static String getthshen(String tiangan, thshen s) {
		String shen = "";
		switch (tiangan) {
		case "甲":
			shen = s.getJia();
			break;
		case "乙":
			shen = s.getYi();
			break;
		case "丙":
			shen = s.getBing();
			break;
		case "丁":
			shen = s.getDing();
			break;
		case "戊":
			shen = s.getWu();
			break;
		case "己":
			shen = s.getJi();
			break;
		case "庚":
			shen = s.getGeng();
			break;
		case "辛":
			shen = s.getXin();
			break;
		case "壬":
			shen = s.getRen();
			break;
		case "癸":
			shen = s.getGui();
			break;
		default:
			break;
		}
		return shen;
	}
	public static Integer getYueFen(String tiangan) {
		int di = 0;
		switch (tiangan) {
		case "寅":
			di = 1;
			break;
		case "卯":
			di = 2;
			break;
		case "辰":
			di = 3;
			break;
		case "巳":
			di = 4;
			break;
		case "午":
			di = 5;
			break;
		case "未":
			di = 6;
			break;
		case "申":
			di = 7;
			break;
		case "酉":
			di = 8;
			break;
		case "戌":
			di = 9;
			break;
		case "亥":
			di = 10;
			break;
		case "子":
			di = 11;
			break;
		case "丑":
			di = 12;
			break;
		default:
			break;
		}
		return di;
	}
	public static String getG(String a) {
		String F=a.substring(0,1);
		return F;
	}
	public static String getZ(String a) {
		String L=a.substring(1);
		return L;
	}
	public static String DFganzhi(Integer a) {
		String L="";
		if (a==0) {
			L="年干";
		}else if (a==1) {
			L="年支";
		}else if (a==2) {
			L="月干";
		}else if (a==3) {
			L="月支";
		}else if (a==4) {
			L="日干";
		}else if (a==5) {
			L="日支";
		}else if (a==6) {
			L="时干";
		}else if (a==7) {
			L="时支";
		}else if (a==10) {
			L="流干";
		}else if (a==11) {
			L="流支";
		}else if (a==8) {
			L="大干";
		}else if (a==9) {
			L="大支";
		}
		return L;
	}
	public static String DFwuxing(String a) {
		if (a.equals("甲")||a.equals("乙")) {
			return "木";
		}else if (a.equals("丙")||a.equals("丁")) {
			return "火";
		}else if (a.equals("戊")||a.equals("己")) {
			return "土";
		}else if (a.equals("庚")||a.equals("辛")) {
			return "金";
		}else if (a.equals("壬")||a.equals("癸")) {
			return "水";
		}else if (a.equals("寅")||a.equals("卯")) {
			return "木";
		}else if (a.equals("巳")||a.equals("午")) {
			return "火";
		}else if (a.equals("申")||a.equals("酉")) {
			return "金";
		}else if (a.equals("亥")||a.equals("子")) {
			return "水";
		}else if (a.equals("辰")||a.equals("未")||a.equals("戌")||a.equals("丑")) {
			return "土";
		}
		
		return "空";
	}
	public static double jutiNumber(String yuefen,String zhi, Percent perc, rizhu bili) {
		double zhanbi=3;		
//		double G=0.734;
//		double Z=0.265;//这个概率是根据八卦中第一次运算，40和46出现的概率。
		double G=1;
		double Z=1;
		double BG=1;//天干能量占比
		double BZ=1;//地支能量占比
//		double BG=GanzhiBili(yuefen, bili).get(0);//天干能量占比
//		double BZ=GanzhiBili(yuefen, bili).get(1);//地支能量占比
		switch (zhi) {//得到干或支的能量，得到干支组合的能量，得到干与干（支与支）之间生克制化的能量
		case "甲":zhanbi=perc.getJia()*G*BG; break;
		case "乙":zhanbi=perc.getYi()*G*BG; break;
		case "丙":zhanbi=perc.getBing()*G*BG; break;
		case "丁":zhanbi=perc.getDing()*G*BG; break;
		case "戊":zhanbi=perc.getWu()*G*BG; break;
		case "己":zhanbi=perc.getJi()*G*BG; break;
		case "庚":zhanbi=perc.getGeng()*G*BG; break;
		case "辛":zhanbi=perc.getXin()*G*BG; break;
		case "壬":zhanbi=perc.getRen()*G*BG; break;
		case "癸":zhanbi=perc.getGui()*G*BG; break;
		case "寅":zhanbi=perc.getYin()*Z*BZ; break;
		case "卯":zhanbi=perc.getMao()*Z*BZ; break;
		case "辰":zhanbi=perc.getChen()*Z*BZ; break;
		case "巳":zhanbi=perc.getSi()*Z*BZ; break;
		case "午":zhanbi=perc.getWuu()*Z*BZ; break;
		case "未":zhanbi=perc.getWei()*Z*BZ; break;
		case "申":zhanbi=perc.getShen()*Z*BZ; break;
		case "酉":zhanbi=perc.getYou()*Z*BZ; break;
		case "戌":zhanbi=perc.getXu()*Z*BZ; break;
		case "亥":zhanbi=perc.getHai()*Z*BZ; break;
		case "子":zhanbi=perc.getZi()*Z*BZ; break;
		case "丑":zhanbi=perc.getChou()*Z*BZ; break;
		default:
			break;
		}
//		System.out.println("数值：\t"+ddddd+" \t"+BG+" \t"+BZ);
//		System.out.println("数值：\t"+String.format("%.2f", zhanbi)+" \t"+BG+" \t"+BZ);
		return zhanbi;
	}
	public static double firstNengliang(String zhi, Percent perc) throws Exception{
		double zhanbi=3;		
		try {
			switch (zhi) {			
			case "甲":zhanbi=perc.getJia(); break;
			case "乙":zhanbi=perc.getYi(); break;
			case "丙":zhanbi=perc.getBing()*1.0; break;
			case "丁":zhanbi=perc.getDing(); break;
			case "戊":zhanbi=perc.getWu()*1.0; break;
			case "己":zhanbi=perc.getJi(); break;
			case "庚":zhanbi=perc.getGeng(); break;
			case "辛":zhanbi=perc.getXin(); break;
			case "壬":zhanbi=perc.getRen(); break;
			case "癸":zhanbi=perc.getGui(); break;
			case "寅":zhanbi=perc.getYin(); break;
			case "卯":zhanbi=perc.getMao(); break;
			case "辰":zhanbi=perc.getChen(); break;
			case "巳":zhanbi=perc.getSi(); break;
			case "午":zhanbi=perc.getWuu(); break;
			case "未":zhanbi=perc.getWei(); break;
			case "申":zhanbi=perc.getShen(); break;
			case "酉":zhanbi=perc.getYou(); break;
			case "戌":zhanbi=perc.getXu(); break;
			case "亥":zhanbi=perc.getHai(); break;
			case "子":zhanbi=perc.getZi(); break;
			case "丑":zhanbi=perc.getChou(); break;
			default:
				break;
			}
		} catch (Exception e) {
			log.info("八字参数错误！");
		}
		return zhanbi;
	}
	public static List<String> formatTwo(Double a,Double b,Double c,Double d,Double e,Double total) {
		List<String> Nnum=new ArrayList<String>();
		Nnum.add(String.format("%.2f", a));
		Nnum.add(String.format("%.2f", b));
		Nnum.add(String.format("%.2f", c));
		Nnum.add(String.format("%.2f", d));
		Nnum.add(String.format("%.2f", e));
		Nnum.add(String.format("%.2f", total));
		return Nnum;
	}
	public static Map<String, Double> wuformatTwo(Double a,Double b,Double c,Double d,Double e) {
		Map<String, Double> Nnum=new HashMap<String, Double>();
		Nnum.put("jin",setTwoNum(a));
		Nnum.put("shui",setTwoNum(b));
		Nnum.put("mu",setTwoNum(c));
		Nnum.put("huo",setTwoNum(d));
		Nnum.put("tu",setTwoNum(e));
		return Nnum;
	}
	public static List<Double> getEightNumber(Double ng,Double nz,Double yg,Double yz,Double rg,Double rz,Double sg,Double sz) {
		List<Double> Nnum=new ArrayList<Double>();
		Nnum.add(setTwoNum(ng));Nnum.add(setTwoNum(nz));
		Nnum.add(setTwoNum(yg));Nnum.add(setTwoNum(yz));
		Nnum.add(setTwoNum(rg));Nnum.add(setTwoNum(rz));
		Nnum.add(setTwoNum(sg));Nnum.add(setTwoNum(sz));
		return Nnum;
	}
	public static List<Double> getTenNumber(Double dg,Double dz,Double ng,Double nz,Double yg,Double yz,Double rg,Double rz,Double sg,Double sz) {
		List<Double> Nnum=new ArrayList<Double>();
		Nnum.add(setTwoNum(ng));Nnum.add(setTwoNum(nz));
		Nnum.add(setTwoNum(yg));Nnum.add(setTwoNum(yz));
		Nnum.add(setTwoNum(rg));Nnum.add(setTwoNum(rz));
		Nnum.add(setTwoNum(sg));Nnum.add(setTwoNum(sz));
		Nnum.add(setTwoNum(dg));Nnum.add(setTwoNum(dz));
		return Nnum;
	}
	public static List<Double> getTwleveNumber(Double lg,Double lz,Double dg,Double dz,Double ng,Double nz,Double yg,Double yz,Double rg,Double rz,Double sg,Double sz) {
		List<Double> Nnum=new ArrayList<Double>();
		Nnum.add(setTwoNum(ng));Nnum.add(setTwoNum(nz));
		Nnum.add(setTwoNum(yg));Nnum.add(setTwoNum(yz));
		Nnum.add(setTwoNum(rg));Nnum.add(setTwoNum(rz));
		Nnum.add(setTwoNum(sg));Nnum.add(setTwoNum(sz));
		Nnum.add(setTwoNum(dg));Nnum.add(setTwoNum(dz));
		Nnum.add(setTwoNum(lg));Nnum.add(setTwoNum(lz));
		return Nnum;
	}
	public static Double setTwoNum(Double a) {
		BigDecimal bg = new BigDecimal(a);
		double ddddd = bg.setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
		return ddddd;
	}
	public static String getYuefen(String yuefen) {
		String BG="";
		if (yuefen.equals("寅")||yuefen.equals("卯")) {
			BG="春";
		}else if (yuefen.equals("巳")||yuefen.equals("午")) {
			BG="夏";
		}else if (yuefen.equals("申")||yuefen.equals("酉")) {
			BG="秋";
		}else if (yuefen.equals("亥")||yuefen.equals("子")) {
			BG="冬";
		}else if (yuefen.equals("辰")||yuefen.equals("未")) {
			BG="春夏土";
		}else if (yuefen.equals("戌")||yuefen.equals("丑")) {
			BG="秋冬土";
		}
		
		return BG;
	}
	public static List<Double> GanzhiBili(String yuefen, rizhu bili){
		double BG=1;//天干能量占比
		double BZ=1;//地支能量占比
		List<Double> ganzhi=new ArrayList<Double>();
//			if (yuefen.equals("春")){
//				BG=bili.getRiganc();BZ=bili.getRizhic();
//			}else if (yuefen.equals("夏")) {
//				BG=bili.getRiganx();BZ=bili.getRizhix();
//			}else if (yuefen.equals("秋")) {
//				BG=bili.getRiganq();BZ=bili.getRizhiq();
//			}else if (yuefen.equals("冬")) {
//				BG=bili.getRigand();BZ=bili.getRizhid();
//			}else if (yuefen.equals("春夏土")) {
//				BG=bili.getRizhits();BZ=bili.getRizhits();
//			}else if (yuefen.equals("秋冬土")) {
//				BG=bili.getRizhits();BZ=bili.getRizhitx();
//			}
		ganzhi.add(BG);
		ganzhi.add(BZ);
		return ganzhi;
	}
	
}
