package findgood;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

import lombok.extern.slf4j.Slf4j;
/**
 * 	这个项目用来算八字玩
 * @author 唐春
 * @version 1.0 2021年8月2日 上午9:55:00 
 * @since JDK 1.8
 *	localhost：8094
 */
@Slf4j
@SpringBootApplication
@MapperScan("findgood.dao*")
@PropertySource("api.properties")
public class Start{
	public static void main(String[] args){
		SpringApplication.run(Start.class, args);
		log.info("系统启动");
	}
}



