package findgood.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import findgood.entity.User;

@Mapper
public interface userMapper extends BaseMapper<User>{
//	// 用户列表
//	@Select("select * from t_user")
//	@Results({ @Result(property = "userName", column = "user_name"),
//			@Result(property = "passWord", column = "pass_word") })
//	List<User> queryAll();
//
//	// 根据id获取user
//	@Select("select * from t_user where id =#{id}")
//	@Results({ @Result(property = "userName", column = "user_name"),
//			@Result(property = "passWord", column = "pass_word") })
//	User findUserById(Integer id);
//
//	// 根据id修改user
//	@Update("update t_user set user_name=#{userName},pass_word =#{passWord},sex=#{sex},birthday=#{birthday} WHERE id = #{id} ")
//	int updateUser(User user);
//
//	// 根据id删除用户
//	@Delete("delete from t_user where id = #{id}")
//	int deleteUserById(Integer id);
}
